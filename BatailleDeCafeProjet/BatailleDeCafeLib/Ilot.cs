﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BatailleDeCafeLib
{
    public class Ilot
    {
        private List<Parcelle> allParcelle;

        /// <summary>
        /// Constructeur d'un Îlot
        /// </summary>
        /// <param name="allParcelle">La liste des Parcelles nécessaire</param>
        public Ilot(List<Parcelle> allParcelle)
        {
            List<Parcelle> defaultAllParcelle = new List<Parcelle>(); 
            defaultAllParcelle.Add(new Parcelle(new List<Unite>(), "error")); //Parcelle par défaut si allParcelle est null

            this.allParcelle = (allParcelle is null) ? defaultAllParcelle : allParcelle;
        }

        /// <summary>
        /// Constructeur de recopie
        /// </summary>
        /// <param name="otherIlot">L'Îlot à recopier</param>
        public Ilot(Ilot otherIlot)
        {
            List<Parcelle> newList = new List<Parcelle>();
            foreach(Parcelle parcelle in otherIlot.allParcelle)
            {
                newList.Add(new Parcelle(parcelle));
            }
            this.allParcelle = newList;
        }

        /// <summary>
        /// Getter de l'attribut allParcelle
        /// </summary>
        /// <returns>allParcelle</returns>
        public List<Parcelle> GetAllParcelle()
        {
            return allParcelle;
        }

        /// <summary>
        /// Getter d'une Unite parmi celle présente dans l'Ilot
        /// </summary>
        /// <param name="coordonnees">Les coordonnees de l'Unite souhaité</param>
        /// <returns></returns>
        public Unite GetUniteAt(Coordonnees coordonnees)
        {
            Unite uniteToReturn = null;
            foreach(Parcelle parcelle in allParcelle)
            {
                foreach(Unite unite in parcelle.GetAllUnite())
                {
                    if (unite.GetCoordonnees() == coordonnees)
                        uniteToReturn = unite;
                }
            }
            return uniteToReturn;
        }

        /// <summary>
        /// Getter du nombre de graine blanche présente sur l'Ilot
        /// </summary>
        /// <returns>nbGraineWhite</returns>
        public int GetNbGraineWhite()
        {
            int nbGraineWhite = 0;
            foreach (Parcelle parcelle in allParcelle)
            {
                nbGraineWhite = nbGraineWhite + parcelle.GetNbGraineWhite();
            }
            return nbGraineWhite;
        }

        /// <summary>
        /// Getter du nombre de graine noire présente sur l'Ilot
        /// </summary>
        /// <returns>nb_graine_black</returns>
        public int GetNbGraineBlack()
        {
            int nbGraineBlack = 0;
            foreach (Parcelle parcelle in allParcelle)
            {
                nbGraineBlack = nbGraineBlack + parcelle.GetNbGraineBlack();
            }
            return nbGraineBlack;
        }

        /// <summary>
        /// Getter du nombre de graine présente sur l'Ilot
        /// </summary>
        /// <returns>nb_graine</returns>
        public int GetNbGraine()
        {
            return GetNbGraineBlack() + GetNbGraineWhite();
        }

        /// <summary>
        /// Getter d'une Parcelle présente sur l'Ilot en fonction de son nom
        /// </summary>
        /// <param name="name">Le nom de la parcelle souhaité</param>
        /// <returns>parcelleToReturn</returns>
        public Parcelle GetParcelle(String name)
        {
            Parcelle parcelleToReturn = null;
            foreach(Parcelle parcelle in allParcelle)
            {
                if (parcelle.GetName() == name)
                    parcelleToReturn = parcelle;
            }
            return parcelleToReturn;
        }

    }
}
