﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace BatailleDeCafeLib
{
    /// <summary>
    /// Fonction créant la partie grâce a partir du décodage de la map
    /// </summary>
    public class GeneratorGameUtils
    {
        private char[,] map;

        public GeneratorGameUtils(char[,] map)
        {
            this.map = map;
        }

        /// <summary>
        /// Méthode ajoutant chaque unite à sa parcelle
        /// </summary>
        /// <param name="parcelles"></param>
        /// <param name="numParcelle"></param>
        /// <param name="xCoord"></param>
        /// <param name="yCoord"></param>
        public void AddUniteToListParcelles(List<Parcelle> parcelles, char numParcelle, int xCoord, int yCoord)
        {
            bool found = false;
            foreach(Parcelle parcelle in parcelles)
            {
                if(parcelle.GetName() == numParcelle.ToString())
                {
                    found = true;
                    parcelle.PushUnite(new Unite(new Coordonnees(xCoord, yCoord), null));
                }
            }
            if (!found)
            {
                Parcelle newParcelle = new Parcelle(numParcelle.ToString());
                newParcelle.PushUnite(new Unite(new Coordonnees(xCoord, yCoord), null));
                parcelles.Add(newParcelle);
            }
        }

        /// <summary>
        /// Méthode ajoutant les frontière des parcelles aux unites
        /// </summary>
        /// <param name="ilot"></param>
        public void AddFrontiereToUnites(Ilot ilot)
        {
            foreach(Parcelle parcelle in ilot.GetAllParcelle())
            {
                foreach(Unite unite in parcelle.GetAllUnite())
                {
                    List<Unite> uniteVoisin = unite.GetVoisins(ilot);
                    for(int indexVoisin = 0; indexVoisin < 4; indexVoisin++) 
                    {
                        if(!(uniteVoisin[indexVoisin] is null))
                        {
                            if (!ReferenceEquals(parcelle, uniteVoisin[indexVoisin].GetParcelle(ilot)))
                            {
                                if (indexVoisin == 0)
                                    unite.PushFrontiere("N");
                                if (indexVoisin == 1)
                                    unite.PushFrontiere("S");
                                if (indexVoisin == 2)
                                    unite.PushFrontiere("E");
                                if (indexVoisin == 3)
                                    unite.PushFrontiere("W");
                            }
                        }   
                    }
                }
            }
        }

        /// <summary>
        /// Fonction générant la game
        /// </summary>
        /// <returns></returns>
        public Game GenerateGame()
        {
            List<Parcelle> parcelles = new List<Parcelle>();
            for(int indexLigne = 0; indexLigne <= 9; indexLigne++)
            {
                for(int indexColonne = 0; indexColonne <= 9; indexColonne++)
                {
                    this.AddUniteToListParcelles(parcelles, this.map[indexLigne, indexColonne], indexLigne, indexColonne);
                }
            }

            Ilot ilot = new Ilot(parcelles);

            this.AddFrontiereToUnites(ilot);

            return new Game(ilot); 
        }
    }
}
