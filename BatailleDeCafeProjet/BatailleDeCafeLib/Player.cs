﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BatailleDeCafeLib
{
    public class Player
    {
        private String name;
        private int points;
        private int grainesRestantes;

        /// <summary>
        /// Constructeur de la classe Player
        /// </summary>
        /// <param name="name">Nom du Player</param>
        public Player(String name)
        {
            this.name = (name is null) ? "" : name;
            this.points = 0;
            this.grainesRestantes = 28;
        }

        /// <summary>
        /// Getter de l'attribut points
        /// </summary>
        /// <returns>points</returns>
        public int GetPoints()
        {
            return points;
        }

        public void SetPoints(int points)
        {
            this.points = points;
        }

        /// <summary>
        /// Getter de l'attribut name
        /// </summary>
        /// <returns>name</returns>
        public String GetName()
        {
            return name;
        }

        /// <summary>
        /// Getter de l'attribut grainesRestantes
        /// </summary>
        /// <returns>grainesRestantes</returns>
        public int GetGrainesRestantes()
        {
            return grainesRestantes;
        }

        public void SetGrainesRestantes(int grainesRestantes)
        {
            this.grainesRestantes = grainesRestantes;
        }

        public void DecreaseGrainesRestantes()
        {
            this.grainesRestantes--;
        }

        /// <summary>
        /// Retourne toutes les Unites que le Player possede
        /// </summary>
        /// <returns>Liste d'Unites</returns>
        public List<Unite> GetAllUnite(Ilot ilot)
        {
            List<Unite> allUnite = new List<Unite>();
            String graineToFound = null;
            if(name == "Blanc")
                graineToFound = "Blanche";
            if (name == "Noir")
                graineToFound = "Noire";
            if(graineToFound is null)
                return null;
            foreach(Parcelle parcelle in ilot.GetAllParcelle()){
                foreach(Unite unite in parcelle.GetAllUnite()){
                    if (unite.GetGraine() == graineToFound)
                        allUnite.Add(unite);
                }
            }
            return allUnite;
        }

        /// <summary>
        /// Retourne toutes les Parcelles que le Player possede
        /// </summary>
        /// <param name="ilot"></param>
        /// <returns>Liste de Parcelles</returns>
        public List<Parcelle> GetAllParcelle(Ilot ilot)
        {
            List<Parcelle> allParcelle = new List<Parcelle>();
            String graineToFound = null;
            if (name == "Blanc")
                graineToFound = "Blanche";
            if (name == "Noir")
                graineToFound = "Noire";
            if (graineToFound is null)
                return null;
            foreach (Parcelle parcelle in ilot.GetAllParcelle())
            {
                int nbUnite = 0;
                foreach (Unite unite in parcelle.GetAllUnite())
                {
                    if (unite.GetGraine() == graineToFound)
                        nbUnite++;
                }
                if(nbUnite >= (parcelle.GetNbUnite()/2+1))
                {
                    allParcelle.Add(parcelle);
                }
            }
            return allParcelle;
        }
    }
}
