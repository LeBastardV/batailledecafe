﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BatailleDeCafeLib
{
    public class IA
    {
        private Unite lastGraineUnite;

        public IA() 
        {
            lastGraineUnite = null;
        }

        /// <summary>
        /// Méthode enregistrant les coordonnées de la dernière graine posée 
        /// </summary>
        /// <param name="unite"></param>
        public void SetLastGraineCoordonnees(Unite unite)
        {
            lastGraineUnite = unite;
        }

        /// <summary>
        /// Méthode retournant toutes les unites où il est possible de jouer à ce tour de jeu en fonction de la position de la dernière graine posée
        /// </summary>
        /// <param name="unite">Unite dont on regarde la validité pour le tour de jeu</param>
        /// <param name="ilot">Carte de jeu</param>
        /// <returns>Boolean</returns>
        public bool UniteIsValid(Unite unite, Ilot ilot)
        {

            if (lastGraineUnite is null)
                return true;

            return ((unite.GetCoordonnees().GetX() == lastGraineUnite.GetCoordonnees().GetX()) || unite.GetCoordonnees().GetY() == lastGraineUnite.GetCoordonnees().GetY()) && !unite.HaveGraine() && !ReferenceEquals(lastGraineUnite.GetParcelle(ilot), unite.GetParcelle(ilot));
        }

        /// <summary>
        /// Méthode retournant les coordonnées du meilleur coup possible afin de le jouer
        /// </summary>
        /// <param name="ilot">Carte de jeu</param>
        /// <returns>Coordonnees</returns>
        public Coordonnees DeterminateTurn(Ilot ilot)
        {
            Coordonnees bestplay = null;
            float valbestplay = 0;

            foreach(Parcelle parcelle in ilot.GetAllParcelle())
            {
                if(parcelle.GetName() != "M" && parcelle.GetName() != "F")
                {
                    foreach (Unite unite in parcelle.GetAllUnite())
                    {
                        if (UniteIsValid(unite, ilot))
                        {
                            float valunite = DeterminateWeightUnite(unite, ilot);
                            if (  valunite >= valbestplay)
                            {
                                bestplay = unite.GetCoordonnees();
                                valbestplay = valunite;
                            };
                        }
                    }
                }   
            }

            return bestplay;
        }

        public float DeterminateWeightUnite(Unite unite, Ilot ilot)
        {
            float valunite = 0;

            // s'il y a égalité sur une parcelle il faut prendre l'avantage
            valunite += DrawOnAParcelle(unite, ilot);

            // jouer le plus au centre prossible
            valunite += Math.Abs(unite.GetCoordonnees().GetX() - 5) / 2;
            valunite += Math.Abs(unite.GetCoordonnees().GetY() - 5) / 2;

            // jouer parcelle impair puis parcelles les plus grande
            valunite += SizeOfParcelle(unite, ilot);

            // arreter de jouer sur une parcelle quand le monopole de cette parcelle est déjà pris
            valunite += ParcelleWithMonopoleAlready(unite, ilot);

            // sécuriser ou bloquer un monopole si possible
            valunite += SecureAParcelleMonopole(unite, ilot);

            // jouer le plus groupé possible
            valunite += FocusOurPlaysOnOneSpot(unite, ilot);

            // avoir le plus grand alignement de graine
            valunite += LongestAlignment(unite, ilot);


            return valunite;
        }

        /// <summary>
        /// Méthode augmentant la valeur de l'unite s'il y a égalité avec l'adversaire sur la parcelle de cette unite
        /// </summary>
        /// <param name="unite">Unite pour laquelle on étudie sa valeur</param>
        /// <param name="ilot">Carte de jeu</param>
        /// <returns>Int</returns>
        public int DrawOnAParcelle(Unite unite, Ilot ilot)
        {
            int weightToAdd = 0;
            if (((unite.GetParcelle(ilot).GetNbGraineBlack() + 1) / (unite.GetParcelle(ilot).GetNbGraineWhite() + 1)) == 1)
            {
                weightToAdd += 10;
            };
            return weightToAdd;
        }

        /// <summary>
        /// Méthode augmentant la valeur de l'unite en fonction de la taille de la parcelle
        /// Les parcelles impaires sont les plus rentables puis ensuite ce sont les parcelles disponibles les plus grandes
        /// </summary>
        /// <param name="unite">Unite pour laquelle on étudie sa valeur</param>
        /// <param name="ilot">Carte de jeu</param>
        /// <returns>Float</returns>
        public float SizeOfParcelle(Unite unite, Ilot ilot)
        {
            float weightToAdd = 0;
            if (unite.GetParcelle(ilot).GetNbUnite() == 3)
            {
                weightToAdd += 1 / 2;
            }
            else
            {
                weightToAdd += (float)unite.GetParcelle(ilot).GetNbUnite() / 18;
            }
            return weightToAdd;
        }

        /// <summary>
        /// Méthode diminuant grandement la valeur de l'unite si le monopole de la parcelle à laquelle elle appartient est déja atteint par un des joueurs
        /// </summary>
        /// <param name="unite">Unite pour laquelle on étudie sa valeur</param>
        /// <param name="ilot">Carte de jeu</param>
        /// <returns>Int</returns>
        public int ParcelleWithMonopoleAlready(Unite unite, Ilot ilot)
        {
            int weightToAdd = 0;
            if ((unite.GetParcelle(ilot).GetNbGraineWhite() / unite.GetParcelle(ilot).GetNbUnite()) > 0.5 || (unite.GetParcelle(ilot).GetNbGraineBlack() / unite.GetParcelle(ilot).GetNbUnite()) > 0.5)
            {
                weightToAdd -= 10;
            }
            return weightToAdd;
        }

        /// <summary>
        /// Méthode augmentant grandement la valeur de l'unité si cette unite permet de sécuriser le monopole d'une parcelle
        /// </summary>
        /// <param name="unite">Unite pour laquelle on étudie sa valeur</param>
        /// <param name="ilot">Carte de jeu</param>
        /// <returns>Int</returns>
        public int SecureAParcelleMonopole(Unite unite, Ilot ilot)
        {
            int weightToAdd = 0;
            if ((unite.GetParcelle(ilot).GetNbGraineWhite() / unite.GetParcelle(ilot).GetNbUnite()) == 0.5 || (unite.GetParcelle(ilot).GetNbGraineBlack() / unite.GetParcelle(ilot).GetNbUnite()) == 0.5)
            {
                weightToAdd += 10;
            }
            return weightToAdd;
        }

        /// <summary>
        /// Méthode augmentant la valeur de l'unite en fonction de si elle est collée à une autre unite que l'on possède déjà
        /// </summary>
        /// <param name="unite">Unite pour laquelle on étudie sa valeur</param>
        /// <param name="ilot">Carte de jeu</param>
        /// <returns>Float</returns>
        public float FocusOurPlaysOnOneSpot(Unite unite, Ilot ilot)
        {
            float weightToAdd = 0;
            foreach (Unite uniteVoisin in unite.GetVoisins(ilot))
            {
                if(!(uniteVoisin is null))
                {
                    if (uniteVoisin.GetGraine() == "Blanche")
                    {
                        if (uniteVoisin.GetParcelle(ilot) != unite.GetParcelle(ilot))
                        {
                            if (uniteVoisin.GetParcelle(ilot).GetNbGraineWhite() / uniteVoisin.GetParcelle(ilot).GetNbUnite() > 0.5)
                            {
                                weightToAdd += 11 / 2;
                            }
                            else
                            {
                                weightToAdd += 5;
                            }
                        }
                        else
                        {
                            weightToAdd += 5 / 2;
                        }
                    }
                }
            }
            return weightToAdd;
        }

        /// <summary>
        /// Méthode augmentant la valeur de l'unite si elle permet de prendre l'avantage sur le plus grand alignement de graine
        /// </summary>
        /// <param name="unite">Unite pour laquelle on étudie sa valeur</param>
        /// <param name="ilot">Carte de jeu</param>
        /// <returns></returns>
        public int LongestAlignment(Unite unite, Ilot ilot)
        {
            int weightToAdd = 0;
            int XlongestWhite = 0;
            int XlongestWhiteX = -1;
            int XlongestBlack = 0;
            int XlongestBlackX = -1;
            for (int X = 0; X < 10; X++)
            {
                int nbgraineblanche = 0;
                int nbgrianenoire = 0;
                for (int Y = 0; Y < 10; Y++)
                {

                    string graine = ilot.GetUniteAt(new Coordonnees(X, Y)).GetGraine();
                    if (graine == "Noire")
                    {
                        nbgrianenoire += 1;
                    }
                    if (graine == "Blanche")
                    {
                        nbgraineblanche += 1;
                    }
                }
                if (nbgraineblanche > XlongestWhite)
                {
                    XlongestWhite = nbgraineblanche;
                    XlongestWhiteX = X;
                }
                if (nbgrianenoire > XlongestBlack)
                {
                    XlongestBlack = nbgrianenoire;
                    XlongestBlackX = X;
                }
            }

            int YlongestWhite = 0;
            int YlongestWhiteY = -1;
            int YlongestBlack = 0;
            int YlongestBlackY = -1;
            for (int Y = 0; Y < 10; Y++)
            {
                int nbgraineblanche = 0;
                int nbgrianenoire = 0;
                for (int X = 0; X < 10; X++)
                {

                    string graine = ilot.GetUniteAt(new Coordonnees(X, Y)).GetGraine();
                    if (graine == "Noire")
                    {
                        nbgrianenoire += 1;
                    }
                    if (graine == "Blanche")
                    {
                        nbgraineblanche += 1;
                    }
                }
                if (nbgraineblanche > YlongestWhite)
                {
                    YlongestWhite = nbgraineblanche;
                    YlongestWhiteY = Y;
                }
                if (nbgrianenoire > YlongestBlack)
                {
                    YlongestBlack = nbgrianenoire;
                    YlongestBlackY = Y;
                }
            }

            if ((XlongestWhite <= XlongestBlack || XlongestWhite <= YlongestBlack) && (YlongestWhite <= YlongestBlack || YlongestWhite <= XlongestBlack))
            {
                if (XlongestWhite <= YlongestWhite)
                {
                    if (unite.GetCoordonnees().GetY() == YlongestWhiteY) { weightToAdd += 2; }
                }
                else
                {
                    if (unite.GetCoordonnees().GetX() == XlongestWhiteX) { weightToAdd += 2; }
                }
            }
            return weightToAdd;
        }
    }
}
