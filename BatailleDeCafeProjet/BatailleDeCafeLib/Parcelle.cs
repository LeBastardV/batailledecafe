﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace BatailleDeCafeLib
{
    public class Parcelle
    {
        List<Unite> allUnite;
        String name;

        /// <summary>
        /// Contructeur d'une Parcelle
        /// </summary>
        /// <param name="name">La neme de la Parcelle</param>
        public Parcelle(String name)
        {
            this.name = name;
            this.allUnite = new List<Unite>();
        }

        /// <summary>
        /// Constructeur d'une Parcelle
        /// </summary>
        /// <param name="allUnite">Toute les Unites que contient la Parcelle</param>
        /// <param name="name">Le name de la Parcelle</param>
        public Parcelle(List<Unite> allUnite, String name)
        {
            List<Unite> default_allUnite = new List<Unite>();
            default_allUnite.Add(new Unite(-1, -1, ""));
            this.allUnite = (allUnite is null) ?  default_allUnite : allUnite;
            this.name = (name is null) ? "" : name;
        }

        /// <summary>
        /// Constructeur de recopie
        /// </summary>
        /// <param name="otherParcelle">Parcelle à recopier</param>
        public Parcelle(Parcelle otherParcelle)
        {
            List<Unite> newList = new List<Unite>();

            foreach(Unite unite in otherParcelle.allUnite)
            {
                newList.Add(new Unite(unite));
            }

            this.allUnite = newList;
            this.name = otherParcelle.name;
        }

        public void PushUnite(Unite unite)
        {
            this.allUnite.Add(unite);
        }

        /// <summary>
        /// Retourne le namebre de graine blanche présente sur la Parcelle
        /// </summary>
        /// <returns>nb_graine_white</returns>
        public int GetNbGraineWhite()
        {
            int nbGraineWhite = 0;
            foreach (Unite unite in allUnite)
                if (unite.HaveGraine())
                    if (unite.GetGraine() == "Blanche")
                        nbGraineWhite++;
            return nbGraineWhite;
        }

        /// <summary>
        /// Retourne le namebre de graine noire présente sur la Parcelle
        /// </summary>
        /// <returns>nb_graine_black</returns>
        public int GetNbGraineBlack()
        {
            int nbGraineBlack = 0;
            foreach (Unite unite in allUnite)
                if (unite.HaveGraine())
                    if (unite.GetGraine() == "Noire")
                        nbGraineBlack++;
            return nbGraineBlack;
        }

        /// <summary>
        /// Retourne le namebre de graine présente sur la Parcelle
        /// </summary>
        /// <returns>nb_graine</returns>
        public int GetNbGraine()
        {
            return GetNbGraineBlack() + GetNbGraineWhite();  
        }

        /// <summary>
        /// Retourne le namebre d'Unite présente sur la Parcelle
        /// </summary>
        /// <returns></returns>
        public int GetNbUnite()
        {
            return allUnite.Count;
        }

        /// <summary>
        /// Getter de l'attribut allUnite
        /// </summary>
        /// <returns>allUnite</returns>
        public List<Unite> GetAllUnite()
        {
            return allUnite;
        }
        
        /// <summary>
        /// Getter de l'attribut name
        /// </summary>
        /// <returns>name</returns>
        public String GetName()
        {
            return name;
        }

        /// <summary>
        /// Méthode indiquant si la Parcelle contient l'unite passé en paramètre
        /// </summary>
        /// <param name="unite">unite à tester</param>
        /// <returns>Boolean</returns>
        public Boolean Contains(Unite unite)
        {
            bool contains = false;
            int nbIter = 0;
            while(contains == false && nbIter < allUnite.Count)
            {
                if (allUnite[nbIter].Equals(unite))
                    contains = true;
                nbIter++;
            }
            return contains;
        }
    }
}
