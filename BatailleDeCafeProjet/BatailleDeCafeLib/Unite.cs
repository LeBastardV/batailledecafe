﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace BatailleDeCafeLib
{
    public class Unite
    {
        private Coordonnees coordonnees;
        private String graine; //Graine doit être égal qu'à "Blanche" ou "Noire"
        //Frontière est composé soit de 'N', soit 'S', soit 'E', soit 'O' ou des combinaisons de ceux-ci (en références aux points cardinaux)
        private String frontiere;

        /// <summary>
        /// Constructeur d'une Unite
        /// </summary>
        /// <param name="coordonnees">Coordonnées de l'Unite</param>
        /// <param name="frontiere">Frontière que possède l'Unite</param>
        public Unite(Coordonnees coordonnees, String frontiere) : this(coordonnees, frontiere, null)
        {}

        /// <summary>
        /// Constructeur d'une Unite
        /// </summary>
        /// <param name="xCoordonnees">Coordonnées X de l'Unite</param>
        /// <param name="yCoordonnees">Coordonnées Y de l'Unite</param>
        /// <param name="frontiere">Frontière que possède l'Unite</param>
        public Unite(int xCoordonnees, int yCoordonnees, String frontiere) : this(new Coordonnees(xCoordonnees, yCoordonnees), frontiere, null)
        {}

        /// <summary>
        /// Constructeur de recopie d'une Unite
        /// </summary>
        /// <param name="otherUnite">Unite à recopier</param>
        public Unite(Unite otherUnite) : this(new Coordonnees(otherUnite.GetCoordonnees()), otherUnite.GetFrontiere(), otherUnite.GetGraine())
        {}

        /// <summary>
        /// Constructeur le plus complet d'une Unite
        /// </summary>
        /// <param name="coordonnees">Coordonnées de l'Unite</param>
        /// <param name="frontiere">Frontière que possède l'Unite</param>
        /// <param name="graine">Graine que possède l'Unite</param>
        public Unite(Coordonnees coordonnees, String frontiere, String graine)
        {
            this.coordonnees = (coordonnees is null) ? new Coordonnees(-1, -1) : coordonnees;
            this.frontiere = (frontiere is null) ? "" : frontiere;
            PutGraine(graine);
        }

        /// <summary>
        /// Getter de l'attribut coordonnees
        /// </summary>
        /// <returns>coordonnees</returns>
        public Coordonnees GetCoordonnees()
        {
            return this.coordonnees;
        }

        /// <summary>
        /// Méthode retournant si l'unite possède une graine ou non
        /// </summary>
        /// <returns>Boolean</returns>
        public Boolean HaveGraine()
        {
            if (this.graine == null || this.graine.Length == 0)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Getter de l'attribut graine
        /// </summary>
        /// <returns>Le nom de la graine</returns>
        public String GetGraine()
        {
            return this.graine;
        }

        public void PushFrontiere(string frontiere)
        {
            this.frontiere += frontiere;
        }

        /// <summary>
        /// Méthode incrémentant une graine sur l'Unite
        /// </summary>
        /// <param name="graine">Nom de la graine ("Blanche" ou "Noire")</param>
        public void PutGraine(String graine)
        {
            if (!this.HaveGraine())
            {
                if (graine != null)
                {
                    if (graine == "Blanche" || graine == "Noire")
                        this.graine = graine;
                    else
                        this.graine = "";
                }
                else
                {
                    this.graine = "";
                }
            }       
        }

        /// <summary>
        /// Retourne une liste d'Unite contenant tout les voisin si il y en a
        /// Le sens des ajouts dans la liste ce fait de façon Nord, Sud, Est puis Ouest
        /// </summary>
        /// <param name="ilot">L'iLot à parcourir</param>
        /// <returns>allVoisins</returns>
        public List<Unite> GetVoisins(Ilot ilot)
        {
            List<Unite> allVoisin = new List<Unite>();
            Unite voisinNord = null;
            if (!((voisinNord = GetVoisinNord(ilot)) is null))
            {
                allVoisin.Add(voisinNord);
            }
            else
            {
                allVoisin.Add(null);
            }
            Unite voisinSud= null;
            if (!((voisinSud = GetVoisinSud(ilot)) is null))
            {
                allVoisin.Add(voisinSud);
            }
            else
            {
                allVoisin.Add(null);
            }
            Unite voisinEst = null;
            if (!((voisinEst = GetVoisinEst(ilot)) is null))
            {
                allVoisin.Add(voisinEst);
            }
            else
            {
                allVoisin.Add(null);
            }
            Unite voisinOuest = null;
            if (!((voisinOuest = GetVoisinOuest(ilot)) is null))
            {
                allVoisin.Add(voisinOuest);
            }
            else
            {
                allVoisin.Add(null);
            }
            return allVoisin;
        }

        /// <summary>
        /// Retourne l'Unite ce situant au dessus
        /// </summary>
        /// <param name="ilot">Ilot à parcourir</param>
        /// <returns>voisinNord</returns>
        public Unite GetVoisinNord(Ilot ilot)
        {
            //On test d'abord si l'Unite actuel n'est pas déjà tout au nord
            //Au quel cas, il ne peut pas avoir de voisin au nord
            if (GetCoordonnees().GetY() != 0)
            {
                Unite voisinNord = null;
                foreach (Parcelle parcelle in ilot.GetAllParcelle())
                {
                    foreach (Unite unite in parcelle.GetAllUnite())
                    {
                        if (unite.GetCoordonnees().GetY() == GetCoordonnees().GetY() && unite.GetCoordonnees().GetX() == GetCoordonnees().GetX() - 1)
                            voisinNord = unite;
                    }
                }
                return voisinNord;
            }
            else
                return null;
            
        }

        /// <summary>
        /// Retourne l'Unite ce situant en dessous
        /// </summary>
        /// <param name="ilot">Ilot à parcourir</param>
        /// <returns>voisinSud</returns>
        public Unite GetVoisinSud(Ilot ilot)
        {
            Unite voisinSud = null;
            foreach (Parcelle parcelle in ilot.GetAllParcelle())
            {
                foreach (Unite unite in parcelle.GetAllUnite())
                {
                    if (unite.GetCoordonnees().GetY() == GetCoordonnees().GetY() && unite.GetCoordonnees().GetX() == GetCoordonnees().GetX() + 1)
                        voisinSud = unite;
                }
            }
            return voisinSud;
        }

        /// <summary>
        /// Retourne l'Unite ce situant à droite
        /// </summary>
        /// <param name="ilot">l'Ilot à parcourir</param>
        /// <returns>voisinEst</returns>
        public Unite GetVoisinEst(Ilot ilot)
        {
            Unite voisinEst = null;
            foreach (Parcelle parcelle in ilot.GetAllParcelle())
            {
                foreach (Unite unite in parcelle.GetAllUnite())
                {
                    if (unite.GetCoordonnees().GetY() == GetCoordonnees().GetY() + 1 && unite.GetCoordonnees().GetX() == GetCoordonnees().GetX())
                        voisinEst = unite;
                }
            }
            return voisinEst;
        }

        /// <summary>
        /// Retourne l'Unite ce situant à gauche
        /// </summary>
        /// <param name="ilot">l'Ilot à parcourir</param>
        /// <returns>voisinOuest</returns>
        public Unite GetVoisinOuest(Ilot ilot)
        {
            Unite voisinOuest = null;
            foreach (Parcelle parcelle in ilot.GetAllParcelle())
            {
                foreach (Unite unite in parcelle.GetAllUnite())
                {
                    if (unite.GetCoordonnees().GetY() == GetCoordonnees().GetY() - 1 && unite.GetCoordonnees().GetX() == GetCoordonnees().GetX())
                        voisinOuest = unite;
                }
            }
            return voisinOuest;
        }

        /// <summary>
        /// Méthode retournant un boolean vrai si l'Unite possède une frontière, sinon faux
        /// </summary>
        /// <returns></returns>
        public Boolean HaveFrontiere()
        {
            if (this.frontiere.Length != 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Getter de l'attribut frontiere
        /// </summary>
        /// <returns>frontiere</returns>
        public String GetFrontiere()
        {
            return this.frontiere;
        }

        /// <summary>
        /// Méthode retournant si une Unite a les même coordonnees qu'une autre
        /// </summary>
        /// <param name="otherUnite">Autre Unite</param>
        /// <returns>Boolean</returns>
        public Boolean SameCoordonnees(Unite otherUnite)
        {
            if (this.coordonnees == otherUnite.coordonnees)
                return true;
            else
                return false;
        }

        public Parcelle GetParcelle(Ilot ilot)
        {
            foreach(Parcelle parcelle in ilot.GetAllParcelle())
            {
                foreach(Unite unite in parcelle.GetAllUnite())
                {
                    if(this == unite)
                    {
                        return parcelle;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Surcharge de l'operateur ==
        /// </summary>
        /// <param name="unite1">Unite à comparer</param>
        /// <param name="unite2">Unite à comparer</param>
        /// <returns>boolean</returns>
        public static bool operator ==(Unite unite1, Unite unite2)
        {
            if(unite1 is null || unite2 is null)
            {
                return false;
            }
            return unite1.GetCoordonnees() == unite2.GetCoordonnees() && unite1.GetFrontiere() == unite2.GetFrontiere() && unite1.GetGraine() == unite2.GetGraine();
        }

        /// <summary>
        /// Surcharge de l'operateur !=
        /// </summary>
        /// <param name="unite1">Unite à comparer</param>
        /// <param name="unite2">Unite à comparer</param>
        /// <returns>boolean</returns>
        public static bool operator !=(Unite unite1, Unite unite2)
        {
            return !(unite1 == unite2);
        }

        /// <summary>
        /// Surcharge de la méthode equals
        /// </summary>
        /// <param name="obj">L'objet à comparer</param>
        /// <returns>boolean</returns>
        public override bool Equals(object obj)
        {
            return obj is Unite && this == (Unite)obj;
        }
    }
}
