﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BatailleDeCafeLib
{
    public class Game
    {
        private Ilot ilot;
        private Player whitePlayer;
        private Player blackPlayer;
        private IA ia;

        /// <summary>
        /// Constructeur de la classe Game
        /// </summary>
        /// <param name="ilot">L'ilot de la partie</param>
        public Game(Ilot ilot)
        {
            this.ilot = ilot;
            this.whitePlayer = new Player("Blanc");
            this.blackPlayer = new Player("Noir");
            this.ia = new IA();
        }

        /// <summary>
        /// Constructeur de la classe Game
        /// </summary>
        /// <param name="allParcelle">Les parcelles que possèdent l'ilot</param>
        public Game(List<Parcelle> allParcelle) : this(new Ilot(allParcelle)) { }

        /// <summary>
        /// Getter de l'attribut Ilot
        /// </summary>
        /// <returns>Ilot</returns>
        public Ilot GetIlot()
        {
            return this.ilot;
        }

        /// <summary>
        /// Getter de l'attribut whitePlayer
        /// </summary>
        /// <returns>Player</returns>
        public Player GetPlayerWhite()
        {
            return this.whitePlayer;
        }

        /// <summary>
        /// Getter de l'attribut blackPlayer
        /// </summary>
        /// <returns>Player</returns>
        public Player GetPlayerBlack()
        {
            return this.blackPlayer;
        }

        /// <summary>
        /// Getter de l'attribut ia
        /// </summary>
        /// <returns>IA</returns>
        public IA GetIa()
        {
            return this.ia;
        }

        /// <summary>
        /// Met à jour l'ilot avec une nouvelle graine reçu par le serveur
        /// </summary>
        /// <param name="coordOfNewGraine">Coordonnée de la graine</param>
        /// <param name="player">Couleur de la graine du joueur (Noire pour l'énnemi, Blanche pour allié) </param>
        public void UpdateGame(Coordonnees coordOfNewGraine, string player)
        {
            ilot.GetUniteAt(coordOfNewGraine).PutGraine(player);

            if (player == "Noire")
                blackPlayer.DecreaseGrainesRestantes();
            if (player == "Blanche")
                whitePlayer.DecreaseGrainesRestantes();

            ia.SetLastGraineCoordonnees(this.ilot.GetUniteAt(coordOfNewGraine));
        }
    }
}
