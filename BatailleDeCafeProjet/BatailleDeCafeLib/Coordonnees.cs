﻿using System;

namespace BatailleDeCafeLib
{
    public class Coordonnees
    {
        private int X;
        private int Y;

        /// <summary>
        /// Constructeur de la classe Coordonnees
        /// </summary>
        /// <param name="X">Coordonnees X</param>
        /// <param name="Y">Coordonnees Y</param>
        public Coordonnees(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }

        /// <summary>
        /// Constructeur de recopie
        /// </summary>
        /// <param name="otherCoordonnees">Prend les paramètres de otherCoordonnees</param>
        public Coordonnees(Coordonnees otherCoordonnees)
        {
            if(otherCoordonnees is null)
            {
                //On met des valeurs par défauts si otherCoordonnees est null
                this.X = -1;
                this.Y = -1;  
            }
            else
            {
                this.X = otherCoordonnees.X;
                this.Y = otherCoordonnees.Y;
            }
        }

        /// <summary>
        /// Getter de l'attribut X
        /// </summary>
        /// <returns>X coordonnees</returns>
        public int GetX()
        {
            return this.X;
        }

        /// <summary>
        /// Getter de l'attribut Y
        /// </summary>
        /// <returns>Y coordonnees</returns>
        public int GetY()
        {
            return this.Y;
        }

        /// <summary>
        /// Fonction permettant de savoir si une coordonnée est voisine de celle-ci
        /// </summary>
        /// <param name="otherCoordonnees"></param>
        /// <returns></returns>
        public Boolean CheckVoisin(Coordonnees otherCoordonnees)
        {
            Boolean voisin = false;
            //On check d'abord de la position X
            if (this.X == otherCoordonnees.X + 1 || this.X == otherCoordonnees.X - 1) voisin = true;
            //On check ensuite la position Y
            if (this.Y == otherCoordonnees.Y + 1 || this.Y == otherCoordonnees.Y - 1) voisin = true;
            return voisin;
        }

        public static bool operator ==(Coordonnees coord1, Coordonnees coord2)
        {
            if (coord1 is null || coord2 is null) return false;
            return coord1.X == coord2.X && coord1.Y == coord2.Y;
        }

        public static bool operator !=(Coordonnees coord1, Coordonnees coord2)
        {
            return !(coord1 == coord2);
        }

        public override bool Equals(object obj)
        {
            return obj is Coordonnees && this == (Coordonnees)obj;
        }
    }
}
