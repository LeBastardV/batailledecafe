using System;
using System.Linq;

namespace BatailleDeCafe
{
    public static class Decodage
    {
        /// <summary>
        /// Une fonction qui permet de séparer la trame dans un tableau de 10*10
        /// </summary>
        /// <param name='trame'>Trame à séparer</param>
        /// <param name='tableauEntier'>Tableau 10*10 contenant les entiers de la trame</param>
        public static void SeparationTrame(string trame, int[,] tableauEntier)
        {
            string[][] tableauString = trame.Split('|').Select(trame => trame.Split(':')).ToArray();
            for (int colonne = 0; colonne < 10; colonne++)
            {
                for (int ligne = 0; ligne < 10; ligne++)
                {
                    tableauEntier[colonne, ligne] = int.Parse(tableauString[colonne][ligne]);
                }
            }
        }
        /// <summary>
        /// Une fonction qui permet de decoder Mer/Foret/Terre
        /// </summary>
        /// <param name='valeur'>Valeur entière de l'unité</param>
        public static char DecoderTerreMerForet(int valeur)
        {
            char lettre = 'T';//'T' par défaut pour Terre (utile au debogage)
            if ((valeur - 64) >= 0)
            {
                valeur = valeur - 64;
                lettre = 'M';
            }
            if ((valeur - 32) >= 0)
            {
                lettre = 'F';
            }
            return lettre;
        }
        /// <summary>
        /// Une fonction qui permet de savoir les frontières d'une unité
        /// </summary>
        /// <param name='valeur'>Valeur entière de l'unité</param>
        /// <param name='frontiere'>Tableau de Booléen indiquant si l'unité à une frontière [Nord,Ouest,Sud,Est]</param>
        public static void DecoderFrontiere(int valeur, ref bool[] frontiere)
        {
            frontiere = new bool[4] { false, false, false, false };//[Nord,Ouest,Sud,Est]
            if ((valeur - 64) >= 0) { valeur = valeur - 64; }
            if ((valeur - 32) >= 0) { valeur = valeur - 32; }
            if (valeur - 8 >= 0)
            {
                valeur = valeur - 8;
                frontiere[3] = true;
            }//Est
            if (valeur - 4 >= 0)
            {
                valeur = valeur - 4;
                frontiere[2] = true;
            }//Sud
            if (valeur - 2 >= 0)
            {
                valeur = valeur - 2;
                frontiere[1] = true;
            }//Ouest
            if (valeur - 1 >= 0)
            {
                valeur = valeur - 1;
                frontiere[0] = true;
            }//Nord
        }
        /// <summary>
        /// Une fonction qui permet de décoder les différentes parcelles de la map
        /// </summary>
        /// <param name='lettreActuelle'>Plus grande lettre utilisée pour identifier les percelles</param>
        /// <param name='map'>Tableau de caractère indiquant les parcelles/Mer/Terre</param>
        /// <param name='ligne'>Identificateur de la ligne où on décode</param>
        /// <param name='colonne'>Identificateur de la colonne où on décode</param>
        /// <param name='frontiere'>Tableau de Booléen indiquant si l'unité à une frontière [Nord,Ouest,Sud,Est]</param>
        /// <param name='tableauEntier'>Tableau d'entier (venant de la trame)</param>
        public static void DecoderParcelle(ref int lettreActuelle, char[,] map, int ligne, int colonne,
            bool[] frontiere, int[,] tableauEntier)
        {
            if (map[ligne, colonne] == 'T')
            {
                if (frontiere[0] && frontiere[1] && !(frontiere[3]))
                {//Nord et Ouest et non Est
                    bool[] tabFrontiereDroite = new bool[4];
                    DecoderFrontiere(tableauEntier[ligne, colonne + 1], ref tabFrontiereDroite);

                    if (!(tabFrontiereDroite[0]))
                    {//Pas de frontiere nord pour l'unité à droite
                        map[ligne, colonne] = map[ligne - 1, colonne + 1];
                    }
                    else
                    {
                        map[ligne, colonne] = Convert.ToChar(lettreActuelle + 1);
                        lettreActuelle = map[ligne, colonne];
                    }
                }
                if (frontiere[0] && frontiere[1] && frontiere[3])
                {//Nord et Ouest et Est
                    map[ligne, colonne] = Convert.ToChar(lettreActuelle + 1);
                    lettreActuelle = map[ligne, colonne];
                }
                if (!(frontiere[1]))
                {//non Ouest
                    map[ligne, colonne] = map[ligne, colonne - 1];
                }
                if (!(frontiere[0]))
                {//non Nord
                    map[ligne, colonne] = map[ligne - 1, colonne];
                }
            }
        }
        /// <summary>
        /// Une fonction qui permet de décoder le tableau de 10*10, utilisant les fonctions ci-dessus
        /// </summary>
        /// <param name='tableau'>Tableau de d'entier (venant de la trame)</param>
        /// <param name='map'>Tableau de caractère indiquant les parcelles/Mer/Terre</param>
        public static void DecoderTableau(int[,] tableau, char[,] map)
        {
            int lettreActuelle = 96;
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    map[ligne, colonne] = DecoderTerreMerForet(tableau[ligne, colonne]);
                    bool[] tabFrontiere = new bool[4];
                    DecoderFrontiere(tableau[ligne, colonne], ref tabFrontiere);
                    DecoderParcelle(ref lettreActuelle, map, ligne, colonne, tabFrontiere, tableau);
                }
            }
        }
    }
}
