﻿using System;
using System.Net.Sockets; //sockets
using System.Text; //UTF8

using BatailleDeCafe;
using BatailleDeCafeLib;

namespace BatailleDeCafeConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket mySocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream,
            ProtocolType.Tcp);

            //IP 51.91.120.237, port 1212
            EchangeServeur.Connexion("127.0.0.1", 1213, mySocket);

            //Reception des données
            byte[] data = new byte[300];
            EchangeServeur.ReceptionDonnee(mySocket, data);

            string trame = Encoding.UTF8.GetString(data);

            int[,] tableauEntier = new int[10, 10];
            Decodage.SeparationTrame(trame, tableauEntier);

            char[,] map = new char[10, 10];
            Decodage.DecoderTableau(tableauEntier, map);

            GeneratorGameUtils generator = new GeneratorGameUtils(map);

            Game game = generator.GenerateGame();

            // for(int ligne=0; ligne<10; ligne++){
            //     for(int colonne=0; colonne<10; colonne++){
            //         Console.Write("{0} ",map[ligne,colonne]);}
            //     Console.WriteLine();}

            Coordonnees coord;
            do
            {
                coord = game.GetIa().DeterminateTurn(game.GetIlot());
            } while (!(EchangeServeur.JouerUnCoup(mySocket, coord, game)));

            EchangeServeur.ReceptionScore(mySocket);
            EchangeServeur.Deconnexion(mySocket);

        }
    }
}
