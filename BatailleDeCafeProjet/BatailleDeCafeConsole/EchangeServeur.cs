using System;
using System.Net; //outils réseau
using System.Net.Sockets; //sockets
using System.Text; //UTF8

using BatailleDeCafeLib;


namespace BatailleDeCafe
{
    /// <summary>
    /// Une classe implémentant le nécessaire afin de se connecter et d'échanger avec le serveur
    /// </summary>
    public static class EchangeServeur
    {
        /// <summary>
        /// Une fonction qui permet de connecter un socket à un serveur
        /// </summary>
        /// <param name='ip'>ip du serveur</param>
        /// <param name='port'>port du serveur</param>
        /// <param name='socket'>socket</param>
        public static void Connexion(string ip, int port, Socket socket)
        {
            IPAddress[] tabIp = Dns.GetHostAddresses(ip);
            try
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Connexion en cours à {0}", ip);
                socket.Connect(tabIp[0], port);
                Console.WriteLine("Connexion établie");
                Console.ForegroundColor = ConsoleColor.White;

            }
            catch (SocketException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
                Console.ForegroundColor = ConsoleColor.White;

                throw;
            }
        }
        /// <summary>
        /// Une fonction qui permet de déconnecter un socket d'un serveur
        /// </summary>
        /// <param name='socket'>socket à déconnecter et détruire</param>
        public static void Deconnexion(Socket socket)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Désactivation en cours");
                socket.Shutdown(SocketShutdown.Both);
                Console.WriteLine("Déstruction de la socket en cours");
                socket.Close();
                Console.WriteLine("Terminé");
                Console.ForegroundColor = ConsoleColor.White;

            }
            catch (SocketException e)
            {
                Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
            }
        }
        /// <summary>
        /// Une fonction qui permet de receptionner les données envoyer par le serveur
        /// </summary>
        /// <param name='socket'>socket connecté au serveur</param>
        /// <param name='data'>données récupérées</param>
        public static void ReceptionDonnee(Socket socket, byte[] data)
        {
            if (socket.Connected)
            {
                try
                {
                    socket.Receive(data);
                }
                catch (SocketException e)
                {
                    Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
                }
            }
            else
            {
                Console.WriteLine("Socket non connecté");
            }
        }
        /// <summary>
        /// Jouer un coup avec les 4 trames serveur
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="coordGraine"></param>
        /// <returns>Booleen si la partie est fini ou non</returns>
        public static bool JouerUnCoup(Socket socket, Coordonnees coordGraine, Game game)
        {
            bool partieFinie = false;
            //Envoie de notre coup
            EnvoieGraine(socket, coordGraine);

            //Recupération si Valide ou non
            byte[] coupValide = new byte[4];
            ReceptionDonnee(socket, coupValide);
            //Si VALI : Stocker
            //Si INVA : Ne Pas Stocker
            if (String.Equals(Encoding.UTF8.GetString(coupValide), "VALI"))
            {
                //Si VALI : Stocker
                game.UpdateGame(coordGraine, "Blanche");
            }

            //Recupération du jeu serveur
            Coordonnees coupServeur;
            coupServeur = ReceptionCoupServeur(socket);
            if (coupServeur.GetX() == -1 && coupServeur.GetY() == -1)//si -1 -1 => serveur envoie FINI
            {
                return true;
            }
            else
            {
                //Stocker coord coupServeur
                game.UpdateGame(coupServeur, "Noire");
            }

            //Recuperation ENCO/FINI
            byte[] fin = new byte[4];
            ReceptionDonnee(socket, fin);
            if (String.Equals(Encoding.UTF8.GetString(fin), "FINI"))
            {
                partieFinie = true;
            }
            return partieFinie;

        }
        /// <summary>
        /// Envoyer les coordonées au serveurs de la position de notre graine
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="coordGraine"></param>
        public static void EnvoieGraine(Socket socket, Coordonnees coordGraine)
        {
            string xCoord = coordGraine.GetX().ToString();
            string yCoord = coordGraine.GetY().ToString();
            string trameCoord = string.Concat("A:", xCoord, yCoord);
            byte[] maGraine = Encoding.UTF8.GetBytes(trameCoord);
            socket.Send(maGraine);
        }
        /// <summary>
        /// Reçois l'emplacement d'une graine
        /// </summary>
        /// <param name="socket"></param>
        /// <returns></returns>
        public static Coordonnees ReceptionCoupServeur(Socket socket)
        {
            byte[] coupServeur = new byte[4];
            ReceptionDonnee(socket, coupServeur);
            int xCoord = -1;
            int yCoord = -1;
            if (!(String.Equals(Encoding.UTF8.GetString(coupServeur), "FINI")))
            {
                try
                {
                    xCoord = Int32.Parse(Encoding.UTF8.GetString(new[] { coupServeur[2] }));
                    yCoord = Int32.Parse(Encoding.UTF8.GetString(new[] { coupServeur[3] }));
                }
                catch (FormatException)
                {
                    Console.WriteLine("Unable to parse coordonnées");
                }
            }
            return new Coordonnees(xCoord, yCoord);
            //Sinon La Partie est FINI
        }


        /// <summary>
        /// Méthode pour recevoir et afficher le score final
        /// </summary>
        /// <param name="socket"></param>
        public static void ReceptionScore(Socket socket)
        {
            byte[] score = new byte[7];
            EchangeServeur.ReceptionDonnee(socket, score);
            System.Console.WriteLine(Encoding.UTF8.GetString(score));
            String[] tabScore = (Encoding.UTF8.GetString(score)).Split(":");
            System.Console.WriteLine("Score Joueur: {0}", tabScore[1]);
            System.Console.WriteLine("Score Serveur: {0}", tabScore[2]);
        }
    }
}
