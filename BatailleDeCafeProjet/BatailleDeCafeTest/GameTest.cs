﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using BatailleDeCafeLib;

namespace BatailleDeCafeTest
{
    public class GameTest
    {
        [Fact]
        public void TestUpdateGame()
        {
            Unite unite1 = new Unite(1, 1, "N");
            Unite unite2 = new Unite(1, 2, "N");
            Unite unite3 = new Unite(1, 3, "N");

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> all_parcelle = new List<Parcelle>();
            all_parcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(all_parcelle);

            Game g1 = new Game(ilot1);
            Assert.Empty(g1.GetPlayerBlack().GetAllUnite(g1.GetIlot()));
            g1.UpdateGame(new Coordonnees(1, 1), "Noire");
            Assert.NotEmpty(g1.GetPlayerBlack().GetAllUnite(g1.GetIlot()));
        }
    }
}
