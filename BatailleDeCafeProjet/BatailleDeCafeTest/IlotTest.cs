﻿using System;
using System.Collections.Generic;
using System.Text;
using BatailleDeCafeLib;
using Xunit;

namespace BatailleDeCafeTest
{
    public class IlotTest
    {
        [Fact]
        public void TestGetUniteAt()
        {
            Unite unite1 = new Unite(1, 1, "N");
            Unite unite2 = new Unite(1, 2, "N");
            Unite unite3 = new Unite(1, 3, "N");

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> all_parcelle = new List<Parcelle>();
            all_parcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(all_parcelle);

            Assert.Equal(unite1, ilot1.GetUniteAt(new Coordonnees(1, 1)));
            Assert.NotEqual(unite1, ilot1.GetUniteAt(new Coordonnees(1, 2)));
            Assert.Equal(unite3, ilot1.GetUniteAt(new Coordonnees(1, 3)));
        }

        [Fact]
        public void TestGetNbGraineWhite()
        {
            Unite unite1 = new Unite(new Coordonnees(1, 1), "N", "Blanche");
            Unite unite2 = new Unite(1, 2, "N");
            Unite unite3 = new Unite(1, 3, "N");

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> all_parcelle = new List<Parcelle>();
            all_parcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(all_parcelle);

            Assert.Equal(1, ilot1.GetNbGraineWhite());
            Assert.Equal(0, ilot1.GetNbGraineBlack());

            unite2.PutGraine("Blanche");
            Assert.Equal(2, ilot1.GetNbGraineWhite());

            unite3.PutGraine("Blanche");
            Assert.Equal(3, ilot1.GetNbGraineWhite());
        }


        [Fact]
        public void TestGetNbGraineBlack()
        {
            Unite unite1 = new Unite(new Coordonnees(1, 1), "N", "Noire");
            Unite unite2 = new Unite(1, 2, "N");
            Unite unite3 = new Unite(1, 3, "N");

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> all_parcelle = new List<Parcelle>();
            all_parcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(all_parcelle);

            Assert.Equal(1, ilot1.GetNbGraineBlack());
            Assert.Equal(0, ilot1.GetNbGraineWhite());

            unite2.PutGraine("Noire");
            Assert.Equal(2, ilot1.GetNbGraineBlack());

            unite3.PutGraine("Noire");
            Assert.Equal(3, ilot1.GetNbGraineBlack());
        }

        [Fact]
        public void TestGetNbGraine()
        {
            Unite unite1 = new Unite(new Coordonnees(1, 1), "N", "Noire");
            Unite unite2 = new Unite(1, 2, "N");
            Unite unite3 = new Unite(1, 3, "N");

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> all_parcelle = new List<Parcelle>();
            all_parcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(all_parcelle);

            Assert.Equal(1, ilot1.GetNbGraine());

            unite2.PutGraine("Blanche");
            Assert.Equal(2, ilot1.GetNbGraine());

            unite3.PutGraine("Noire");
            Assert.Equal(3, ilot1.GetNbGraine());
        }

        [Fact]
        public void TestGetParcelle()
        {
            Unite unite1 = new Unite(new Coordonnees(1, 1), "N", "Noire");
            Unite unite2 = new Unite(1, 2, "N");
            Unite unite3 = new Unite(1, 3, "N");

            //On crée 3 Parcelles avec 1 Unite à l'intérieur chacune
            List<Unite> allUnite1 = new List<Unite>();
            List<Unite> allUnite2 = new List<Unite>();
            List<Unite> allUnite3 = new List<Unite>();

            allUnite1.Add(unite1); allUnite2.Add(unite2); allUnite3.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite1, "a");
            Parcelle parcelle2 = new Parcelle(allUnite2, "b");
            Parcelle parcelle3 = new Parcelle(allUnite3, "c");

            List<Parcelle> all_parcelle = new List<Parcelle>();
            all_parcelle.Add(parcelle1);    all_parcelle.Add(parcelle2);    all_parcelle.Add(parcelle3);
            //Puis on met ces Parcelles dans l'Ilot
            Ilot ilot1 = new Ilot(all_parcelle);

            Assert.Equal(parcelle1, ilot1.GetParcelle("a"));
            Assert.Equal(parcelle2, ilot1.GetParcelle("b"));
            Assert.Equal(parcelle3, ilot1.GetParcelle("c"));

            Assert.NotEqual(parcelle1, ilot1.GetParcelle("c"));
            Assert.NotEqual(parcelle2, ilot1.GetParcelle("c"));
            Assert.NotEqual(parcelle3, ilot1.GetParcelle("a"));
        }
    }
}
