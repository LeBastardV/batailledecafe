﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using BatailleDeCafeLib;

namespace BatailleDeCafeTest
{
    public class ParcelleTest
    {
        [Fact]
        public void TestGetNbGraineWhite()
        {
            Unite unite1 = new Unite(1, 1, "N");
            Unite unite2 = new Unite(1, 2, "N");
            Unite unite3 = new Unite(1, 3, "N");

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1);  allUnite.Add(unite2);  allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");
            Assert.Equal(0, parcelle1.GetNbGraineWhite());

            parcelle1.GetAllUnite()[0].PutGraine("Blanche");
            Assert.Equal(1, parcelle1.GetNbGraineWhite());

            parcelle1.GetAllUnite()[1].PutGraine("Noire");
            Assert.Equal(1, parcelle1.GetNbGraineBlack());
            Assert.Equal(1, parcelle1.GetNbGraineWhite());

            parcelle1.GetAllUnite()[1].PutGraine("Blanc");
            Assert.Equal(1, parcelle1.GetNbGraineWhite());

            parcelle1.GetAllUnite()[2].PutGraine("Blanche");
            Assert.Equal(2, parcelle1.GetNbGraineWhite());
        }

        [Fact]
        public void TestGetNbGraineBlack()
        {
            Unite unite1 = new Unite(1, 1, "");
            Unite unite2 = new Unite(1, 2, "");
            Unite unite3 = new Unite(1, 3, null);

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");
            Assert.Equal(0, parcelle1.GetNbGraineBlack());

            parcelle1.GetAllUnite()[0].PutGraine("Noire");
            Assert.Equal(1, parcelle1.GetNbGraineBlack());

            parcelle1.GetAllUnite()[1].PutGraine("Noire");
            Assert.Equal(2, parcelle1.GetNbGraineBlack());

            parcelle1.GetAllUnite()[2].PutGraine("Blanche");
            Assert.Equal(2, parcelle1.GetNbGraineBlack());
        }

        [Fact]
        public void TestGetNbGraine()
        {
            Unite unite1 = new Unite(1, 1, "");
            Unite unite2 = new Unite(1, 2, "");
            Unite unite3 = new Unite(1, 3, null);

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");
            Assert.Equal(0, parcelle1.GetNbGraine());

            parcelle1.GetAllUnite()[0].PutGraine("Noire");
            Assert.Equal(1, parcelle1.GetNbGraine());

            parcelle1.GetAllUnite()[1].PutGraine("Blanche");
            Assert.Equal(2, parcelle1.GetNbGraine());

            parcelle1.GetAllUnite()[2].PutGraine("Blanc");
            Assert.Equal(2, parcelle1.GetNbGraine());
        }

        [Fact]
        public void TestContains()
        {
            Unite unite1 = new Unite(1, 1, "");
            Unite unite2 = new Unite(1, 2, "");
            Unite unite3 = new Unite(1, 3, null);

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");
            Assert.True(parcelle1.Contains(unite1));

            Unite unite4 = new Unite(2, 2, "");
            Assert.False(parcelle1.Contains(unite4));

            //Unite 5 == Unite2
            Unite unite5 = new Unite(1, 2, "");
            Assert.True(parcelle1.Contains(unite5));
        }
    }
}
