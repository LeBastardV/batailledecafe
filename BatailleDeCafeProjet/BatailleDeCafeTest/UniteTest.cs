﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using BatailleDeCafeLib;

namespace BatailleDeCafeTest
{
    public class UniteTest
    {
        [Fact]
        public void TestHaveGraine()
        {
            Unite unite1 = new Unite(new Coordonnees(1, 1), "N", "Blanche");
            Unite unite2 = new Unite(1, 1, "N");

            Assert.True(unite1.HaveGraine());
            Assert.False(unite2.HaveGraine());
            unite2.PutGraine("Noire");
            Assert.True(unite2.HaveGraine());
        }
        
        [Fact]
        public void TestPutGraine()
        {
            Unite unite1 = new Unite(1, 1, "N");
            Unite unite2 = new Unite(1, 2, "N");

            unite1.PutGraine("Blanche");
            unite2.PutGraine("Blanc");

            //Seul "Blanche" et "Noire" sont prises en compte

            Assert.True(unite1.HaveGraine());
            Assert.False(unite2.HaveGraine());

            unite2.PutGraine("Noir");

            Assert.False(unite2.HaveGraine());

            unite2.PutGraine("Noire");

            Assert.True(unite2.HaveGraine());
        }

        [Fact]
        public void TestHaveFrontiere()
        {
            Unite unite1 = new Unite(1, 1, "N");
            Unite unite2 = new Unite(1, 1, "");

            Assert.True(unite1.HaveFrontiere());
            Assert.False(unite2.HaveFrontiere());
        }

        [Fact]
        public void TestSameCoordonnees()
        {
            Unite unite1 = new Unite(1, 1, "N");
            Unite unite2 = new Unite(1, 1, "NE");

            Assert.True(unite1.SameCoordonnees(unite2));

            Unite unite3 = new Unite(1, 2, "");

            Assert.False(unite1.SameCoordonnees(unite3));
            Assert.False(unite3.SameCoordonnees(unite2));
        }

        [Fact]
        public void TestGetVoisinNord()
        {
            Unite unite1 = new Unite(1, 1, null);
            Unite unite2 = new Unite(2, 1, null);
            Unite unite3 = new Unite(3, 1, null);

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> allParcelle = new List<Parcelle>();
            allParcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(allParcelle);

            Assert.Null(unite1.GetVoisinNord(ilot1));
            Assert.Equal(unite1, unite2.GetVoisinNord(ilot1));
            Assert.Equal(unite2, unite3.GetVoisinNord(ilot1));
        }

        [Fact]
        public void TestGetVoisinSud()
        {
            Unite unite1 = new Unite(1, 1, null);
            Unite unite2 = new Unite(2, 1, null);
            Unite unite3 = new Unite(3, 1, null);

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> allParcelle = new List<Parcelle>();
            allParcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(allParcelle);

            Assert.Null(unite3.GetVoisinSud(ilot1));
            Assert.Equal(unite3, unite2.GetVoisinSud(ilot1));
            Assert.Equal(unite2, unite1.GetVoisinSud(ilot1));
        }

        [Fact]
        public void TestGetVoisinEst()
        {
            Unite unite1 = new Unite(1, 1, null);
            Unite unite2 = new Unite(2, 1, null);
            Unite unite3 = new Unite(3, 1, null);
            Unite unite4 = new Unite(1, 2, null);

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3); allUnite.Add(unite4);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> allParcelle = new List<Parcelle>();
            allParcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(allParcelle);

            Assert.Null(unite3.GetVoisinEst(ilot1));
            Assert.Null(unite2.GetVoisinEst(ilot1));

            Assert.Equal(unite4, unite1.GetVoisinEst(ilot1));
        }

        [Fact]
        public void TestGetVoisinOuest()
        {
            Unite unite1 = new Unite(1, 1, null);
            Unite unite2 = new Unite(2, 1, null);
            Unite unite3 = new Unite(3, 1, null);
            Unite unite4 = new Unite(1, 0, null);

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3); allUnite.Add(unite4);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> allParcelle = new List<Parcelle>();
            allParcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(allParcelle);

            Assert.Null(unite2.GetVoisinOuest(ilot1));
            Assert.Null(unite3.GetVoisinOuest(ilot1));

            Assert.Equal(unite4, unite1.GetVoisinOuest(ilot1));
        }

        [Fact]
        public void TestGetVoisin()
        {
            Unite unite1 = new Unite(1, 1, null);
            Unite unite2 = new Unite(2, 1, null);
            Unite unite3 = new Unite(3, 1, null);
            Unite unite4 = new Unite(1, 2, null);
            Unite unite5 = new Unite(3, 3, null);


            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3); allUnite.Add(unite4); allUnite.Add(unite5);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> allParcelle = new List<Parcelle>();
            allParcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(allParcelle);

            //Unite2 a 1 voisin à droite et un autre à gauche
            Assert.Equal(unite1, unite2.GetVoisins(ilot1)[0]);
            Assert.Equal(unite3, unite2.GetVoisins(ilot1)[1]);

            //Unite1 a 1 voisin à droite et un autre en dessous
            Assert.Equal(unite4, unite1.GetVoisins(ilot1)[2]);
            Assert.Equal(unite2, unite1.GetVoisins(ilot1)[1]);

            //Unite5 a aucun voisin
            for(int index = 0; index < 4; index++)
            {
                Assert.Null(unite5.GetVoisins(ilot1)[index]);
            }
        }

        [Fact]
        public void TestOperatorEqual()
        {
            Unite unite1 = new Unite(1, 1, "NO");
            Unite unite2 = new Unite(1, 1, "NO");
            Unite unite3 = new Unite(3, 1, "N");

            Assert.False(unite1 == unite3);
            Assert.False(unite2 == unite3);
            Assert.True(unite1 == unite2);

            //Coordonnees identique mais pas les mêmes fonctière
            Unite unite4 = new Unite(1, 1, "");

            Assert.False(unite1 == unite4);
            Assert.False(unite2 == unite4);
        }

        [Fact]
        public void TestOperatorDiff()
        {
            Unite unite1 = new Unite(1, 1, "NO");
            Unite unite2 = new Unite(1, 1, "NO");
            Unite unite3 = new Unite(3, 1, "N");

            Assert.True(unite1 != unite3);
            Assert.True(unite2 != unite3);
            Assert.False(unite1 != unite2);

            //Coordonnees identique mais pas les mêmes fonctière
            Unite unite4 = new Unite(1, 1, "");

            Assert.True(unite1 != unite4);
            Assert.True(unite2 != unite4);
        }

        [Fact]
        public void TestEquals()
        {
            Unite unite1 = new Unite(1, 1, "NO");
            Unite unite2 = new Unite(1, 1, "NO");
            Unite unite3 = new Unite(3, 1, "N");

            Assert.False(unite1.Equals(unite3));
            Assert.False(unite2.Equals(unite3));
            Assert.True(unite1.Equals(unite2));

            //Coordonnees identique mais pas les mêmes fonctière
            Unite unite4 = new Unite(1, 1, "");

            Assert.False(unite1.Equals(unite4));
            Assert.False(unite2.Equals(unite4));
        }
    }
}
