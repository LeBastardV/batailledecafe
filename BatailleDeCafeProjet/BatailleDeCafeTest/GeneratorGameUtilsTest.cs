﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using BatailleDeCafeLib;

namespace BatailleDeCafeTest
{
    public class GenratorGameUtilsTest
    {
        [Fact]
        public void TestAddUniteToListParcelles()
        {
            List<Parcelle> parcelles = new List<Parcelle>();

            GeneratorGameUtils generator = new GeneratorGameUtils(null);

            generator.AddUniteToListParcelles(parcelles, 'a', 0, 0);
            generator.AddUniteToListParcelles(parcelles, 'a', 0, 1);
            generator.AddUniteToListParcelles(parcelles, 'a', 0, 2);

            Assert.Single(parcelles);

            generator.AddUniteToListParcelles(parcelles, 'b', 0, 3);
            generator.AddUniteToListParcelles(parcelles, 'b', 0, 4);

            Assert.Equal(2, parcelles.Count);

            generator.AddUniteToListParcelles(parcelles, 'c', 0, 5);
            generator.AddUniteToListParcelles(parcelles, 'c', 0, 6);

            Assert.Equal(3, parcelles.Count);


            generator.AddUniteToListParcelles(parcelles, 'a', 1, 0);
            generator.AddUniteToListParcelles(parcelles, 'a', 1, 1);
            generator.AddUniteToListParcelles(parcelles, 'a', 1, 2);

            generator.AddUniteToListParcelles(parcelles, 'b', 1, 3);
            generator.AddUniteToListParcelles(parcelles, 'b', 1, 4);

            generator.AddUniteToListParcelles(parcelles, 'c', 1, 5);
            generator.AddUniteToListParcelles(parcelles, 'c', 1, 6);

            Assert.Equal(3, parcelles.Count);

        }

        [Fact]
        public void TestAddFrontiereToUnites()
        {
            List<Parcelle> parcelles = new List<Parcelle>();
            GeneratorGameUtils generator = new GeneratorGameUtils(null);

            Unite unite1 = new Unite(1, 1, null);
            Unite unite4 = new Unite(1, 2, null);
            Unite unite2 = new Unite(2, 1, null);
            Unite unite5 = new Unite(2, 2, null);
            Unite unite3 = new Unite(3, 1, null);
            Unite unite6 = new Unite(3, 2, null);

            List<Unite> allUnite1 = new List<Unite>();
            allUnite1.Add(unite1); allUnite1.Add(unite2); allUnite1.Add(unite3);
            Parcelle parcelle1 = new Parcelle(allUnite1, "a");

            List<Unite> allUnite2 = new List<Unite>();
            allUnite2.Add(unite4); allUnite2.Add(unite5); allUnite2.Add(unite6);
            Parcelle parcelle2 = new Parcelle(allUnite2, "b");

            List<Parcelle> all_parcelle = new List<Parcelle>();
            all_parcelle.Add(parcelle1);
            all_parcelle.Add(parcelle2);

            Ilot ilot1 = new Ilot(all_parcelle);

            generator.AddFrontiereToUnites(ilot1);

            Assert.Equal("E", unite1.GetFrontiere());
            Assert.Equal("E", unite2.GetFrontiere());
            Assert.Equal("E", unite3.GetFrontiere());
            Assert.Equal("W", unite4.GetFrontiere());
            Assert.Equal("W", unite5.GetFrontiere());
            Assert.Equal("W", unite6.GetFrontiere());
        }

        [Fact]
        public void TestGenerateGame()
        {
            List<Parcelle> parcelles = new List<Parcelle>();
            char[,] map = {
                { 'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd', 'd' },
                { 'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd', 'd' },
                { 'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd', 'd' },
                { 'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd', 'd' },
                { 'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd', 'd' },
                { 'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd', 'd' },
                { 'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd', 'd' },
                { 'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd', 'd' },
                { 'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd', 'd' },
                { 'a', 'a', 'a', 'b', 'b', 'c', 'c', 'c', 'd', 'd' },
            };
            GeneratorGameUtils generator = new GeneratorGameUtils(map);

            Game game = generator.GenerateGame();

            Assert.Equal(0, game.GetPlayerBlack().GetPoints());
            Assert.Equal(0, game.GetPlayerWhite().GetPoints());

            Assert.Equal(4, game.GetIlot().GetAllParcelle().Count);
        }
    }
}
