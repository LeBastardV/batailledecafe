using System;
using Xunit;

using BatailleDeCafe;

namespace BatailleDeCafeTest
{
    public class TestDecodage
    {
        [Fact]
        public void TestDecodageTerreMerForet()
        {
            Assert.Equal('M',Decodage.DecoderTerreMerForet(64));
            Assert.Equal('F',Decodage.DecoderTerreMerForet(32));
            Assert.Equal('T',Decodage.DecoderTerreMerForet(2));

        }
        [Fact]
        public void TestDecodageFrontiere()
        {
            bool[] defaut = new bool[4] {false,false,false,false};//[Nord,Ouest,Sud,Est]
            bool[] tabFrontiere = new bool[4];
            Decodage.DecoderFrontiere(0,ref tabFrontiere);
            Assert.Equal(defaut, tabFrontiere);

            defaut = new bool[4] {false,true,true,false};//[Nord,Ouest,Sud,Est]
            Decodage.DecoderFrontiere(70,ref tabFrontiere);
            Assert.Equal(defaut, tabFrontiere);
        }
    }
}
