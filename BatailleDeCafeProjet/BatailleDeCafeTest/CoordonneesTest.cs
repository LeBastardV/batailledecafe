﻿using System;
using System.Collections.Generic;
using Xunit;
using BatailleDeCafeLib;

namespace BatailleDeCafeTest
{
    public class CoordonneesTest
    {
        [Fact]
        public void TestCheckVoisin()
        {
            Coordonnees coord1 = new Coordonnees(1, 1);
            Coordonnees coord2 = new Coordonnees(2, 1);
            Coordonnees coord3 = new Coordonnees(3, 1);

            Assert.True(coord1.CheckVoisin(coord2));
            Assert.True(coord2.CheckVoisin(coord3));

            Assert.False(coord2.CheckVoisin(coord2));
            Assert.False(coord1.CheckVoisin(coord3));

            Assert.False(coord3.CheckVoisin(coord1));
        }

        [Fact]
        public void TestOperateurDoubleEqual()
        {
            Coordonnees coord1 = new Coordonnees(1, 1);
            Coordonnees coord2 = new Coordonnees(coord1);
            Coordonnees coord3 = new Coordonnees(3, 1);

            Assert.True((coord1 == coord2));
            Assert.True((coord2 == coord1));
            Assert.False((coord1 == coord3));
            Assert.False((coord3 == coord2));
        }

        [Fact]
        public void TestOperateurDiff()
        {
            Coordonnees coord1 = new Coordonnees(1, 1);
            Coordonnees coord2 = new Coordonnees(coord1);
            Coordonnees coord3 = new Coordonnees(3, 1);

            Assert.False((coord1 != coord2));
            Assert.False((coord2 != coord1));
            Assert.True((coord1 != coord3));
            Assert.True((coord3 != coord2));
        }

        [Fact]
        public void TestEquals()
        {
            Coordonnees coord1 = new Coordonnees(1, 1);
            Coordonnees coord2 = new Coordonnees(coord1);
            Coordonnees coord3 = new Coordonnees(3, 1);

            Assert.True(coord1.Equals(coord2));
            Assert.True(coord2.Equals(coord1));
            Assert.False(coord1.Equals(coord3));
            Assert.False(coord3.Equals(coord2));
        }
    }
}
