using System;
using Xunit;
using BatailleDeCafeLib;
using System.Collections.Generic;

namespace BatailleDeCafeTest
{
    public class PlayerTest
    {
        [Fact]
        public void TestGetAllUnite()
        {
            Unite unite1 = new Unite(0, 0, "N");
            unite1.PutGraine("Blanche");
            Unite unite2 = new Unite(0, 1, "N");
            unite2.PutGraine("Noire");
            Unite unite3 = new Unite(0, 2, "N");
            unite3.PutGraine("Blanche");

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> all_parcelle = new List<Parcelle>();
            all_parcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(all_parcelle);

            Player p1 = new Player("Blanc");
            Player p2 = new Player("Noir");

            Assert.Equal(unite1, p1.GetAllUnite(ilot1)[0]);
            Assert.Equal(unite3, p1.GetAllUnite(ilot1)[1]);
            Assert.Equal(unite2, p2.GetAllUnite(ilot1)[0]);
        }

        [Fact]
        public void TestGetAllParcelle()
        {
            Unite unite1 = new Unite(0, 0, "N");
            unite1.PutGraine("Blanche");
            Unite unite2 = new Unite(0, 1, "N");
            unite2.PutGraine("Noire");
            Unite unite3 = new Unite(0, 2, "N");
            unite3.PutGraine("Blanche");
            Unite unite4 = new Unite(0, 3, "N");

            List<Unite> allUnite = new List<Unite>();

            allUnite.Add(unite1); allUnite.Add(unite2); allUnite.Add(unite3);

            Parcelle parcelle1 = new Parcelle(allUnite, "a");

            List<Parcelle> all_parcelle = new List<Parcelle>();
            all_parcelle.Add(parcelle1);
            Ilot ilot1 = new Ilot(all_parcelle);

            Player p1 = new Player("Blanc");
            Player p2 = new Player("Noir");

            Assert.Equal(parcelle1, p1.GetAllParcelle(ilot1)[0]);
            Assert.Empty(p2.GetAllParcelle(ilot1));
        }
    }
}
